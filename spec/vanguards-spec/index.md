# Tor Vanguards Specification

<a id="vanguards-spec.txt-1"></a>

## Introduction and motivation

A guard discovery attack allows attackers to determine the guard relay of a
Tor client. The hidden service protocol provides an attack vector for a guard
discovery attack since anyone can force an HS to construct a 3-hop circuit to
a relay, and repeat this process until one of the adversary's middle relays
eventually ends up chosen in a circuit. These attacks are also possible to
perform against clients, by causing an application to make repeated
connections to multiple unique onion services.

The adversary must use a protocol side channel to confirm that their relay was
chosen in this position (see [Proposal
#344](../proposals/344-protocol-info-leaks.txt)), and then learns the guard
relay of a client, or service.

When a successful guard discovery attack is followed with compromise or
coercion of the guard relay, the onion service or onion client can be
deanonymized. Alternatively, Netflow analytics data purchase can be (and has
been) used for this deanonymization, without interacting with the Guard relay
directly (see again [Proposal #344](../proposals/344-protocol-info-leaks.txt)).

This specification assumes that Tor protocol side channels have 100% accuracy
and are undetectable, for simplicity in [reasoning about expected attack
times](./vanguards-stats.md). Presently, such 100% accurate
side channels exist in silent form, in the Tor Protocol itself.

As work on addressing Tor's protocol side channels progresses, these attacks
will require application-layer activity that can be monitored and managed by
service operators, as opposed to silent and unobservable side channel activity
via the Tor Protocol itself. Application-layer side channels are also expected
to have less accuracy than native Tor protocol side channels, due to the
possibility of false positives caused by similar application activity
elsewhere on the Tor network. Despite this, we will preserve the original
assumption of 100% accuracy, for simplicity of explanation.

## Overview

In this specification, we specify two forms of a multi-layered Guard system:
one for long-lived services, called Full Vanguards, and one for onion clients
and short-lived services, called Vanguards-Lite.

Both approaches use a mesh topology, where circuits can be created from any
relay in a preceding layer to any relay in a subsequent layer.

The core difference between these two mechanisms is that Full Vanguards has
two additional layers of fixed vanguard relays, which results in longer path
lengths and higher latency. In contrast, Vanguards-Lite has only one
additional layer of fixed vanguard relays, and preserves the original path
lengths in use by onion services. Thus, Full Vanguards comes with a
performance cost, where as Vanguards-Lite does not. The rotation periods
of the two approaches also differ.

Vanguards-Lite MUST be the default for all onion service and onion client
activity; Full Vanguards SHOULD be available as an optional configuration
option for services.

Neither system applies to Exit activity.

### Terminology

Tor's original guards are called First Layer Guards.

The first layer of vanguards is at the second hop, and is called the
Second Layer Guards.

The second layer of vanguards is at the third hop, and is called the Third
Layer Guards.

A circuit stem is the beginning portion of a hidden service circuit that is
common to all hidden service circuit types, under a given vanguards behavior
option (none, lite, or full). Abstracting this common portion is useful for
circuit prebuilding, to maintain a pool of circuit prefixes (stems) that can
be used for any hidden service circuit.

> Note: In Arti, this circuit stem prefix is further abstracted, to handle
> the fact that some hidden circuit types require an additional random middle
> node, where as others do not (see diagrams below). Other than easing circuit
> pre-building, this distinction has no consequence on paths. The below diagrams
> are canonical behavior for each given circuit type.

### Visualizing Full Vanguards

Full Vanguards pins these two middle positions into a mesh topology, where any
relay in a layer can be used in that position in a circuit, as follows:

```text
                       -> vanguard_2A
                                      -> vanguard_3A
          -> guard_1A  -> vanguard_2B -> vanguard_3B
       HS                             -> vanguard_3C
          -> guard_1B  -> vanguard_2C -> vanguard_3D
                                      -> vanguard_3E
                       -> vanguard_2D -> vanguard_3F
```

Additionally, to avoid trivial discovery of the third layer, and to minimize
linkability, we insert an extra middle relay after the third layer guard for
client side intro and hsdir circuits, and service-side rendezvous circuits.
This means that the set of paths for Client (C) and Service (S) side look like
this:

```text
     Client hsdir:  C - G - L2 - L3 - M - HSDIR
     Client intro:  C - G - L2 - L3 - M - I
     Client rend:   C - G - L2 - L3 - R
     Service hsdir: S - G - L2 - L3 - HSDIR
     Service intro: S - G - L2 - L3 - I
     Service rend:  S - G - L2 - L3 - M - R
```

### Visualizing Vanguards-Lite

Vanguards-Lite uses only one layer of vanguards:

```text
                       -> vanguard_2A

          -> guard_1A  -> vanguard_2B
       HS
          -> guard_1B  -> vanguard_2C

                       -> vanguard_2D
```

This yields shorter path lengths, of the following form:

```text
     Client hsdir:  C -> G -> L2 -> M -> HSDir
     Client intro:  C -> G -> L2 -> M -> Intro
     Client rend:   C -> G -> L2 -> Rend
     Service hsdir: C -> G -> L2 -> M -> HSDir
     Service intro: C -> G -> L2 -> M -> Intro
     Service rend:  C -> G -> L2 -> M -> Rend
```

## Alternatives

An alternative to vanguards for client activity is to restrict the number of
onion services that a Tor client is allowed to connect to, in a certain period
of time. This defense was explored in [Onion Not
Found](https://petsymposium.org/2022/files/papers/issue1/popets-2022-0026.pdf).

We have opted not to deploy this defense, for three reasons:

  1. It does not generalize to the service-side of onion services
  2. Setting appropriate rate limits on the number of onion service content
elements on a page for Tor Browser is difficult. Sites like Facebook use
multiple onion domains for various content elements on a single page.
  3. It is even more difficult to limit the number of service connections for
  arbitrary applications, such as cryptocurrency wallets, mining, and other
  distributed apps deployed on top of onion services that connect to multiple
  services (such as Ricochet).

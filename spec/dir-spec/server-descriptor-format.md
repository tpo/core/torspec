<a id="dir-spec.txt-2.1.1"></a>

# Server descriptor format

Server descriptors consist of the following items.

In lines that take multiple arguments, extra arguments SHOULD be
accepted and ignored.  Many of the nonterminals below are defined in
section 2.1.3.

Note that many versions of Tor will generate an extra newline at the
end of their descriptors.  Implementations MUST tolerate one or
more blank lines at the end of a single descriptor or a list of
concatenated descriptors.  New implementations SHOULD NOT generate
such blank lines.

## Server descriptor items

### `router` --- Introduce a router descriptor {#item:router}

  * **`router` _nickname_ _address_ _ORPort_ _SOCKSPort_ _DirPort_ ..**
  * At start, exactly once.

Indicates the beginning of a server descriptor.

**_nickname_** must be a
valid router nickname as specified in section 2.1.3.
**_address_** must
be an IPv4
address in dotted-quad format.

The last three numbers indicate the
TCP ports at which this OR exposes functionality. **_ORPort_** is a port at
which this OR accepts TLS connections for the main OR protocol;
**_SOCKSPort_** is deprecated and should always be 0;
and **_DirPort_** is the
port at which this OR accepts directory-related HTTP connections.  If
any port is not supported, the value 0 is given instead of a port
number.  (At least one of _DirPort_ and _ORPort_ SHOULD be set;
authorities MAY reject any descriptor with both _DirPort_ and _ORPort_ of
0.)

### `identity-ed25519` --- Specify the router's ed25519 identity {#item:identity-ed25519}

  * **`identity-ed25519`**
  * _certificate_ Object, `ED25519 CERT`
  * Exactly once, in second position in document.
  * No extra arguments.

**_certificate_** is an [Ed25519 certificate]
with terminating =s removed from its base64-encoding.
<!-- TODO ^ should this remark about ='s be in the meta-format doc? -->
<!-- TODO give the actual name of the key -->

When this element
is present, it MUST appear as the first or second element in
the router descriptor.
<!-- TODO ^ remove this which is redundant and contradictory -->

_certificate_ has CERT_TYPE of \[04\].
It must include a [signed-with-ed25519-key] extension
so that we can extract the master identity key.

\[Before Tor 0.4.5.1-alpha, this field was optional.\]

### `master-key-ed25519` --- Redundantly specify the router's ed25519 identity {#item:master-key-ed25519}

 * **`master-key-ed25519` _MasterKey_ ..**
 * Exactly once.

Contains the base-64 encoded ed25519 master key
as a single argument.
If it is present,
it MUST match the identity key in `identity-ed25519`.

<!-- TODO give the actual name of the key -->
<!-- TODO #294 why both this and identity-ed25519? -->

\[Before Tor 0.4.5.1-alpha, this field was optional.\]

### `bandwidth` --- Report router's network bandwidth {#item:bandwidth}

 * **`bandwidth` _bandwidth-avg_ _bandwidth-burst_ _bandwidth-observed_ ..**
 * Exactly once.
 * Each argument is a number in decimal, in bytes per second.

Estimated bandwidth for this router.
**_bandwidth-avg_** is the volume
that the OR is willing to sustain over long periods;
**_bandwidth-burst_** is the volume that
the OR is willing to sustain in very short intervals.
<!-- These two figures aren't estimates, are they?  They're declarations -->

**_bandwidth-observed_** is an estimate of the capacity
this relay can handle:
The relay remembers the max bandwidth sustained output
over any ten second period in the past 5 days,
and another sustained input.
_bandwidth-observed_ value is the lesser of these two numbers.

Tor versions released before 2018 only kept bandwidth-observed
for one day.
These versions are no longer supported or recommended.
<!-- TODO maybe this historical note should be deleted? -->

### `platform` --- Describe the platform on which this relay is running {#item:platform}

 * **`platform`** _string_...
 * _string_ is the [whole rest of the line]
 * At most once

A human-readable string describing the system on which this OR is
running.  This MAY include the operating system, and SHOULD include
the name and version of the software implementing the Tor protocol.

### `published` --- Time this descriptor (and extra-info) was generated {#item:published}

 * **`published`** _date_ _time_ ..
 * _date_ _time_ is _YYYY_-_MM_-_DD_ _HH_:_MM_:_SS_ and is in UTC
   <!-- TODO #282 -->
 * Exactly once

When this descriptor (and its corresponding
extra-info document if any) was generated.

### `fingerprint` -- Redundant hash of ASN-1-encoding of router identity key {#item:fingerprint}

 * **`fingerprint`** _fingerprint_ ..
 * _fingerprint_ is multiple arguments --- see below.
 * At most once

**_fingerprint_** is
the hash `SHA1(DER(KP_relayid_rsa))`,
encoded in hex,
with a single space after every 4 characters.
A descriptor is considered invalid
(and MUST be rejected)
if the fingerprint line does not match the public key.

<!-- TODO #294 why this and also `signing-key`? -->
<!-- TODO #288 correct and clarify -->

### `hibernating` --- Whether the relay is hibernating {#item:hibernating}

 * **`hibernating`** bool ..
 * At most once

If the value is 1, then the Tor relay was hibernating when the
descriptor was published, and shouldn't be used to build circuits.

### `uptime` --- How long this relay has been continuously running {#item:uptime}

 * **`uptime`** number
 * At most once

The number of seconds that this OR process has been running.

<!-- TODO don't talk about processes, talk about behaviour -->

### `onion-key` --- Relay's obsolete RSA TAP key {#item:onion-key}

 * **`onion-key`**
 * _key_ Object, `RSA PUBLIC KEY`
 * At most once
 * No extra arguments

This element MUST be present if `onion-key-crosscert` is present.
Relays MUST generate this element
unless the [`publish-dummy-tap-key`] network parameter is set to 0.

<!-- TODO #294 -->

This obsolete RSA key was once used used to encrypt CREATE cells.
It is no longer used. The key, if present, MUST be 1024 bits.
Clients SHOULD validate this element if it is provided.

**_key_** is a DER PKCS#1 RSAPublicKey structure encoded as an Object.

<!-- TODO state the actual key name -->

### `onion-key-crosscert` --- Reverse signature by obsolete TAP key {#item:onion-key-crosscert}

 * **`onion-key-crosscert`**
 * _signature_ cert NL a RSA signature in PEM format.
 * At most once
 * No extra arguments

This element MUST be present if onion-key is present.
Clients SHOULD validate this element if it is provided.

This element contains an RSA signature, generated using the
onion-key, of the following:

```text
          A SHA1 hash of the RSA identity key KP_relayid_rsa
             from "signing-key" (see below) [20 bytes]
          The Ed25519 identity key KP_relayid_ed
             from "master-key-ed25519" [32 bytes]
```

If there is no Ed25519 identity key, or if in some future version
there is no RSA identity key, the corresponding field must be
zero-filled.

Parties verifying this signature MUST allow additional data
beyond the 52 bytes listed above.

This signature proves that the party creating the descriptor
had control over the secret key corresponding to the
onion-key.

\[Before Tor 0.4.5.1-alpha, this field was optional whenever
identity-ed25519 was absent.\]

<!-- TODO #294 -->

### **`ntor-onion-key`** -- KP_ntor, the circuit extension key {#item:ntor-onion-key}

 * `ntor-onion-key` _KP_ntor_ ..
 * _KP_ntor_ is [base64] (and MAY have the `=`-padding).
 * Exactly once

**_KP_ntor_** is
the curve25519 public key used for the ntor circuit extended handshake.
<!-- TODO ntor or ntor-v3 or both? Needs an xref, too. -->
<!-- TODO much of this text should be moved to relay-keys.md, with xref -->
The key MUST be accepted for at least 1 week after
any new key is published
in a subsequent descriptor.
<!-- TODO Unclear.  Who must accept it?
     What if they don't have an old descriptor? -->

<!-- TODO #294 we should forbid =-padding -->

\[Before Tor 0.4.5.1-alpha, this field was optional.\]

<!-- TODO ^ this is historical I think? -->

### `ntor-onion-key-crosscert` --- Reverse cert by K_ntor on KP_relayid_ed {#item:ntor-onion-key-crosscert}

 * **`ntor-onion-key-crosscert`** _Bit_ ..
 * _certificate_, Object `ED25519 CERT`
 * Exactly once
 * No extra arguments

**_certificate_** is an [Ed25519 certificate]
created with the ntor-onion-key,
with type \[0a\].
The signed key here is the master identity key.

<!-- TODO use key names in the text -->

**_Bit_** must be "0" or "1".
It indicates the sign of the ed25519 public key
corresponding to the ntor onion key.
If _Bit_ is "0",
then implementations MUST guarantee that
the x-coordinate of the resulting ed25519 public key is positive.
Otherwise, if _Bit_ is "1",
then the sign of the x-coordinate MUST be negative.

<!-- TODO ^ this phrasing about Bit seems weird to me,
     as if there might be multiple choices of ed25519 key.
	 Also "implementations" is unclear: do we mean the relay
	 (generating the descriptor) or the verifying parties?
     The ref to the Appendix should be a link. -->

To compute the ed25519 public key corresponding to a curve25519 key,
and for further explanation on key formats, see appendix C.

This signature proves that the party creating the descriptor
had control over the secret key corresponding to the ntor-onion-key.

<!-- TODO ^ Want non-normative text explaining why that's needed -->

\[Before Tor 0.4.5.1-alpha, this field was optional whenever
identity-ed25519 was absent.\]

<!-- TODO ^ this is historical I think? -->

### `signing-key` --- KP_relayid_rsa, relay's obsolete RSA identity key {#item:signing-key}

 * **`signing-key`**
 * _KP_relayid_rsa_, Object `RSA PUBLIC KEY`
 * Exactly once
 * No extra arguments

**_KP_relayid_rsa_** is the OR's long-term RSA identity key.
It MUST be 1024 bits.

The encoding is as for "onion-key" above.

<!-- TODO #294 is this historical yet? -->
<!-- TODO if not, the format description should be here,
          not cross-referenceing to an obsolete item:
	      ie, the xref/content should be swapped. -->

### (Server descriptor items in ad-hoc representation)

"accept" exitpattern NL
"reject" exitpattern NL

\[Any number\]

These lines describe an "exit policy": the rules that an OR follows
when deciding whether to allow a new stream to a given address.  The
'exitpattern' syntax is described below.  There MUST be at least one
such entry.  The rules are considered in order; if no rule matches,
the address will be accepted.  For clarity, the last such entry SHOULD
be accept *:* or reject *:*.

"ipv6-policy" SP ("accept" / "reject") SP PortList NL

\[At most once.\]

An exit-policy summary as specified in sections 3.4.1 and 3.8.2,
summarizing
the router's rules for connecting to IPv6 addresses. A missing
"ipv6-policy" line is equivalent to "ipv6-policy reject
1-65535".

"overload-general" SP version SP YYYY-MM-DD HH:MM:SS NL

\[At most once.\]

Indicates that a relay has reached an "overloaded state" which can be
one or many of the following load metrics:

```text
         - Any OOM invocation due to memory pressure
         - Any ntor onionskins are dropped
         - TCP port exhaustion
```

The timestamp is when at least one metrics was detected. It should always
be at the hour and thus, as an example, "2020-01-10 13:00:00" is an
expected timestamp. Because this is a binary state, if the line is
present, we consider that it was hit at the very least once somewhere
between the provided timestamp and the "published" timestamp of the
document which is when the document was generated.

The overload-general line should remain in place for 72 hours since last
triggered.  If the limits are reached again in this period, the timestamp
is updated, and this 72 hour period restarts.

The 'version' field is set to '1' for now.

```text
      (Introduced in tor-0.4.6.1-alpha, but moved from extra-info to general
       descriptor in tor-0.4.6.2-alpha)

    "router-sig-ed25519" SP Signature NL

       [Exactly once.]
```

It MUST be the next-to-last element in the descriptor, appearing
immediately before the RSA signature. It MUST contain an Ed25519
signature of a SHA256 digest of the entire document. This digest is
taken from the first character up to and including the first space
after the "router-sig-ed25519" string. Before computing the digest,
the string "Tor router descriptor signature v1" is prefixed to the
document.

The signature is encoded in Base64, with terminating =s removed.

The signing key in the identity-ed25519 certificate MUST
be the one used to sign the document.

\[Before Tor 0.4.5.1-alpha, this field was optional whenever
identity-ed25519 was absent.\]

"router-signature" NL Signature NL

\[At end, exactly once\]
\[No extra arguments\]

The "SIGNATURE" object contains an RSA signature of the PKCS1-padded
SHA1 hash of the entire server descriptor, taken from the beginning of the
"router" line, through the newline after the "router-signature" line.
The server descriptor is invalid unless the signature is performed
with the router's identity key `KP_relayid_rsa`.

### `contact` --- Server administrator contact information {#item:contact}

 * `contact` INFO....
 * _INFO_ is the [whole rest of the line]
 * At most once

[whole rest of the line]: netdoc.md#rest-of-line

Describes a way to contact the relay's administrator, preferably
including an email address and a PGP key fingerprint.

**_INFO_** is starts with the first non-whitespace
after the whitespace after `contact`,
and is the whole rest of the line up to but not including the newline.
<!-- TODO document this situation in the metaformat, giving it a name -->

<!-- TODO say that INFO is not interpreted by computers, only humans? -->

### `bridge-distribution-request` --- Request distribution by BridgeDB {#item:bridge-distribution-request}

 * **`bridge-distribution-request`** _Method_ ..
 * At most once
 * Bridges only

**_Method_** describes how a Bridge address is distributed by BridgeDB.
Recognized methods are:

 * **`none`** ---
   BridgeDB will avoid distributing your bridge address;
 * **`any``** ---
   BridgeDB will choose how to distribute your bridge address;
 * **`https`** ---
   specifies
   distribution via the web interface at
   https://bridges.torproject.org;
 * **`email`** ---
   specifies
   distribution via the email autoresponder at
   bridges@torproject.org;
 * **`moat`** ---
   specifies
   distribution via an interactive menu inside Tor Browser.

Potential future *Method*s must be as follows:

```text
Method = (KeywordChar | "_") +
```

All bridges SHOULD include this line.
Non-bridges MUST NOT include it.

BridgeDB SHOULD treat unrecognized Method values as if they were "none".

<!-- TODO reference some documentation that describes what a bridge is -->

(Default: "any")

<!-- TODO #302 ^ what does this mean?
     Non-bridges don't include this line and bridges SHOULD include it.
     Seems related to: How do we determine whether a relay is a bridge? -->

\[This line was introduced in 0.3.2.3-alpha, with a minimal backport
to 0.2.5.16, 0.2.8.17, 0.2.9.14, 0.3.0.13, 0.3.1.9, and later.\]

### `family` --- Group relays for the purposes of path selectdion {#item:family}

 * **`family`** _name_ _name_ ..
 * One or more _name_ arguments
 * At most once

<!-- TODO is zero name arguments allowed? -->
<!-- TODO I'm assuming the hold text, which said "space-separated",
          was wrong, and that the separator is [\t ]+ -->

Each **_name_**_is a relay nickname,
or [hexdigest](nonterminals-server-descriptors.md).
If two ORs list one another in their `family` entries,
then OPs should treat them as a single OR for the purpose of path selection.

For example, if node A's descriptor contains `family B`,
and node B's descriptor contains `family A`,
then node A and node B should never be used on the same circuit.

### `eventdns` --- Declare support for non-obsolete DNS logic

"eventdns" bool NL

\[At most once\]

Declare whether this version of Tor is using the newer enhanced
dns logic.  Versions of Tor with this field set to false SHOULD NOT
be used for reverse hostname lookups.

```text
        [This option is obsolete.  All Tor current relays should be presumed
         to have the evdns backend.]

   "caches-extra-info" NL
```

\[At most once.\]
\[No extra arguments\]

Present only if this router is a directory cache that provides
extra-info documents.

\[Versions before 0.2.0.1-alpha don't recognize this\]

<!-- TODO #294 eventdns is obsolete -->

### `extra-info-digest` --- Hash of the extra-info document {#item:extra-info-digest}

 * **`extra-info-digest`** _sha1-digest_ \[ _sha256-digest_ \] ..
 * At most once

**_sha1-digest_** is the
SHA1 digest,
hex-encoded
using upper-case characters,
of the router's extra-info document,
as signed in the router's extra-info (that is, not including the signature).

If this Item is absent,
the router does not have an extra-info document.
<!-- TODO are extra-info documents still truly optional? -->

**_sha256-digest_** is the
SHA256 digest,
base64-encoded,
of the extra-info document.
Unlike the _sha1-digest_,
this digest is calculated over the entire document,
including the signature.

<!-- TODO #288 does this base64 involve padding? -->
<!-- TODO #294 _sha1-digest_ should be retired somehow -->
<!-- TODO #294 _sha256-digest_ should be (and maybe is?) non-optional -->

> The difference in the inputs to the two digests
> is due to a long-lived bug in the tor implementation
> that it would be difficult to roll out an incremental fix for,
> not a design choice.
> Future digest algorithms specified
> should not include the signature in the data used to compute the digest.

\[Versions before 0.2.7.2-alpha did not include a SHA256 digest.\]
\[Versions before 0.2.0.1-alpha don't recognize this field at all.\]

### `hidden-service-dir` --- Declares this router to be a Hidden Service directory {#item:hidden-service-dir}

 * **`hidden-service-dir`** ..
 * At most once

Present only if this router stores and serves hidden service descriptors.
This router supports the descriptor versions declared
in the HSDir "proto" entry.
If there is no "proto" entry, this router supports version 2 descriptors.

### `allow-single-hop-exits` --- Declare support for single-hop exit circuits {#item:allow-single-hop-exits}

"allow-single-hop-exits" NL

\[At most once.\]
\[No extra arguments\]

Present only if the router allows single-hop circuits to make exit
connections.  Most Tor relays do not support this: this is
included for specialized controllers designed to support perspective
access and such. This is obsolete in tor version >= 0.3.1.0-alpha.

<!-- TODO #924 this should be made historical if it isn't already -->

### `or-address` -- Alternative ORport address and port {#item:or-address}

"or-address" SP ADDRESS ":" PORT NL

<!-- TODO anomalous argument format; it should be space-separated -->

\[Any number\]

ADDRESS = IP6ADDR | IP4ADDR
IPV6ADDR = an ipv6 address, surrounded by square brackets.
IPV4ADDR = an ipv4 address, represented as a dotted quad.
PORT = a number between 1 and 65535 inclusive.

An alternative for the address and ORPort of the "router" line, but with
two added capabilities:

```text
         * or-address can be either an IPv4 or IPv6 address
         * or-address allows for multiple ORPorts and addresses
```

A descriptor SHOULD NOT include an or-address line that does nothing but
duplicate the address:port pair from its "router" line.

The ordering of or-address lines and their PORT entries matter because
Tor MAY accept a limited number of address/port pairs. As of
Tor 0.2.3.x only the first address/port pair is advertised and used.

<!-- TODO #294 everyone only uses one of these but we must allow several? -->
<!-- TODO AFAICT this is now *only* needed to support IPv6?
          And *only* because `router` isn't specxified to allow IPv6?
          #294 ideally, abolish this in favour of allowing v6 in `router` -->

### `tunnelled-dir-server` --- Accepts BEGIN_DIR relay message via ORport

"tunnelled-dir-server" NL

\[At most once.\]
\[No extra arguments\]

```text
       Present if the router accepts "tunneled" directory requests using a
       BEGIN_DIR relay message over the router's OR port.
          (Added in 0.2.8.1-alpha. Before this, Tor relays accepted
          tunneled directory requests only if they had a DirPort open,
          or if they were bridges.)
```

<!-- TODO #294 apparently everyone supports this, Item should be retired -->

### `proto` - Protocol versions supported {#item:proto}

 * `proto` _entry_ _entry_ ..
 * Exactly once.
 * Zero or more _entry_ arguments.

<!-- TODO discuss "zero or more, unordered" in the metaformat description -->

Syntax of each _entry_:

```
entry = Keyword "=" Values

Values =
Values = Value
Values = Value "," Values

Value = Int
Value = Int "-" Int

Int = NON_ZERO_DIGIT
Int = Int DIGIT
```

Each **_entry_** indicates that the Tor relay supports
one or more versions of the protocol in question.
Entries should be sorted by keyword.
Values should be numerically ascending within each entry.
(This implies that there should be no overlapping ranges.)
Ranges should be represented as compactly as possible.
_Int_s must be no larger than 63.

<!-- TODO define what Keyword is and organise xrefs, defining semantics -->
<!-- TODO should all these "should" be "must"? -->

This field was first added in Tor 0.2.9.x.

\[Before Tor 0.4.5.1-alpha, this field was optional.\]

[`publish-dummy-tap-key`]: ../param-spec.md#publish-dummy-tap-key

[base64]: ../intro/conventions.html#binascii
[Ed25519 certificate]: ../cert-spec.md#ed-certs
[signed-with-ed25519-key]: ../cert-spec.md#signed-with-ed25519

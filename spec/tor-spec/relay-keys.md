<a id="tor-spec.txt-1.1"></a>

# Relay keys and identities

Every Tor relay has multiple public/private keypairs,
with different lifetimes and purposes.
We explain them here.

Each key here has an English name
(like "Ed25519 identity key")
and an unambiguous identifier
(like `KP_relayid_ed`).

In an identifier,
a `KP_` prefix denotes a public key,
and a `KS_` prefix denotes the corresponding secret key.

> For historical reasons or reasons of space,
> you will sometimes encounter
> multiple English names for the same key,
> or shortened versions of that name.
> The identifier for a key, however,
> should always be unique and unambiguous.

For security reasons, **all keys MUST be distinct**:
the same key or keypair should never be used
for separate roles within the Tor protocol suite,
unless specifically stated.
For example,
a relay's identity key `KP_relayid_ed`
MUST NOT also be used
as its medium-term signing key `KP_relaysign_ed`.

## Identity keys {#identity}

An **identity key** is a long-lived key
that uniquely identifies a relay.
Two relays with the same set of identity keys
are considered to be the same;
any relay that changes its identity key
is considered to have become a different relay.

An identity keypair's lifetime
is the same as the lifetime of the relay.

Two identity keys are currently defined:

- <span id="relayid_ed">`KP_relayid_ed`, `KS_relayid_ed`:</span>
   An "ed25519 identity key",
   also sometimes called a "master identity key".

   This is an Ed25519 key.
   This key never expires.
   It is used for only one purpose:
   signing the `KP_relaysign_ed` key,
   which is used to sign
   other important keys and objects.

- <span id="relayid_rsa">`KP_relayid_rsa`, `KS_relayid_rsa`:</span>
   A _legacy_ "RSA identity key".

   This is an RSA key.
   It never expires.
   It is always 1024 bits long,
   and (as discussed [above](./preliminaries.md#ciphers))
   its exponent must be 65537.
   It is used to sign directory documents and certificates.

Note that because the legacy RSA identity key is so short,
it should not be assumed secure against an attacker.
It exists for legacy purposes only.
When authenticating a relay,
a failure to prove an expected RSA identity
is sufficient evidence of a _failure_ to authenticate,
but a successful proof of an RSA identity is not sufficient
to establish a relay's identity.
Parties SHOULD NOT use the RSA identity on its own.

We write `KP_relayid` to refer to a key which is either
`KP_relayid_rsa` or `KP_relayid_ed`.

## Online signing keys {#online-signing}

Since Tor's design tries to support
keeping the high-value Ed25519 relay identity key offline,
we need a corresponding key that can be used for online signing:

- <span id="relaysign_ed">`KP_relaysign_ed`, `KS_relaysign_ed`:</span>
  A medium-term Ed25519 "signing" key.
  This key is signed by the identity key `KP_relayid_ed`,
  and must be kept online.
  A new one should be generated periodically.
  It signs nearly everything else,
  including directory objects,
  and certificates for other keys.

When this key is generated,
it needs to be signed with the `KP_relayid_ed` key,
producing a [certificate of type `IDENTITY_V_SIGNING`](../cert-spec.md).
The `KP_relayid_ed` key is not used for anything else.


## Circuit extension keys

Each relay has one or more **circuit extension keys**
(also called "onion keys").
When [creating](./create-created-cells.md)
or [extending](./create-created-cells.md#EXTEND)
a circuit,
a client uses this key to perform
a [one-way authenticated key exchange][cacr2011-goldberg]
with the target relay.
If the recipient does not have the correct private key,
the handshake will fail.

[cacr2011-goldberg]: https://cacr.uwaterloo.ca/techreports/2011/cacr2011-11.pdf

Circuit extension keys have moderate lifetimes,
on the order of weeks.
They are published as part of the directory protocol,
and relays SHOULD accept handshakes for a while
after publishing any new key.
(The exact durations for these are set via
[a set of network parameters](../param-spec.md#onion-key-rotation-days).)

There are two current kinds of circuit extension keys:

- <span id="ntor">`KP_ntor`, `KS_ntor`</span>:
  A curve25519 key
  used for the [`ntor`](./create-created-cells.md#ntor)
  and [`ntorv3`](./create-created-cells.md#ntor-v3)
  circuit extension handshakes.

- <span id="onion_tap">`KP_onion_tap`, `KS_onion_tap`</span>:
  A 1024 bit RSA key
  used for the obsolete [`TAP`](./create-created-cells.md#ntor)
  circuit extension handshake.

## Family keys {#family}

When a group of relays are controlled by the same operator(s),
we call them a "family".
A family has a keypair:

- `KP_familyid_ed`, `KS_familyid_id`:
  An ed25519 key used to prove membership in a family.

> As of 10 Feburary 2025, this is not yet fully implemented; see
> [proposal 321](../proposals/321-happy-families.md).
>
> This section is here in order to reserve its name.

## Channel authentication {#auth}

There are other keys that relays use to authenticate
as part of their [channel negotiation handshakes](./channels.md).

These keys are authenticated with other, longer lived keys.
Relays MAY rotate them as often as they like,
and SHOULD rotate them frequently—typically, at least once a day.

- <span id="link_ed">`KP_link_ed`, `KS_link_ed`</span>.
  A short-term Ed25519 "link authentication" key, used to authenticate
  the link handshake: see
  ["Negotiating and initializing channels"](./negotiating-channels.md#negotiating).
  This key is signed by the "signing" key, and should be regenerated frequently.

### Legacy channel authentication {#auth-legacy}

These key types were used in
[older versions](./obsolete-channels.md)
of the channel negotiation handshakes.

- <span id="legacy_linkauth_rsa">`KP_legacy_linkauth_rsa`, `KS_legacy_linkauth_rsa`</span>:
  A 1024-bit RSA key, used to authenticate the link handshake.
  (No longer used in modern Tor.)
  It played a role similar to `KP_link_ed`.


As a convenience, to describe legacy versions of the link handshake,
we give a name to the public key used for the TLS handshake itself:

- <span id="legacy_conn_tls">`KP_legacy_conn_tls`, `KS_legacy_conn_tls`</span>:
  A short term key used to for TLS connections.
  (No longer used in modern Tor.)
  This was another name for the server's TLS key,
  which at the time was required to be an RSA key.
  It was used in some legacy handshake versions.

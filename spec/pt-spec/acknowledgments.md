<a id="pt-spec.txt-6"></a>

# Acknowledgments

This specification draws heavily from prior versions done by Jacob
Appelbaum, Nick Mathewson, and George Kadianakis.

<a id="pt-spec.txt-4"></a>

# Anonymity Considerations

When designing and implementing a Pluggable Transport, care
should be taken to preserve the privacy of clients and to avoid
leaking personally identifying information.

Examples of client related considerations are:

- Not logging client IP addresses to disk.

- Not leaking DNS addresses except when necessary.

```text
     - Ensuring that "TOR_PT_PROXY"'s "fail closed" behavior is
       implemented correctly.
```

Additionally, certain obfuscation mechanisms rely on information
such as the server IP address/port being confidential, so clients
also need to take care to preserve server side information
confidential when applicable.

```
Filename: 350-remove-tap.md
Title: A phased plan to remove TAP onion keys
Author: Nick Mathewson
Created: 31 May 2024
Status: Accepted
```

## Introduction

Back in [proposal 216], we introduced the `ntor` circuit extension handshake.
It replaced the older `TAP` handshake, which was badly designed,
and dependent on insecure key lengths (RSA1024, DH1024).

With the [final shutdown of v2 onion services][hsv2-deprecation],
there are no longer any supported users of TAP anywhere in the Tor protocols.
Anecdotally, a relay operator reports that fewer than
one handshake in 300,000 is currently TAP.
(Such handshakes are presumably coming from long-obsolete clients.)

Nonetheless, we continue to bear burdens from TAP support.
For example:
  - TAP keys compose a significant (but useless!) portion
    of directory traffic.
  - The TAP handshake requires cryptographic primitives
    used nowhere else in the Tor protocols.

Now that we are implementing [relays in Arti],
the time is ripe to remove TAP.
(The only alternative is to add a useless TAP implementation in Arti,
which would be a waste of time.)

This document outlines a plan to completely remove the TAP handshake,
and its associated keys, from the Tor protocols.

This is, in some respects, a modernized version of [proposal 245].

## Migration plan

Here is the plan in summary;
we'll discuss each phase below.

- Phase 1: Remove TAP dependencies
  - Item 1: Stop accepting TAP circuit requests.
  - Item 2: Make TAP keys optional in directory documents.
  - Item 3: Publish dummy TAP keys.
- Phase 2: After everybody has updated
  - Item 1: Allow TAP-free routerdescs at the authorities
  - Item 2: Generate TAP-free microdescriptors
- Phase 3: Stop publishing dummy TAP keys.

Phase 1 can begin immediately.

Phase 2 can begin once all supported clients and relays have upgraded
to run versions with the changes made in Phase 1.

Phase 3 can begin once all authorities have made the changes
described in phase 2.


### Phase 1, Item 1: Stop accepting TAP circuit requests.

(All items in phase 1 can happen in parallel.)

Immediately, Tor relays should stop accepting TAP requests.
This includes all CREATE cells (not CREATE2),
and any CREATE2 cell whose type is TAP (0x0000).

When receiving such a request,
relays should respond with DESTROY.
Relays MAY just drop the request entirely, however,
if they find that they are getting too many requests.

Such relays must stop reporting `Relay=1`
among their supported protocol versions.
(This protocol version is not currently required or recommended.)

> If this proposal is accepted,
> we should clarify the protocol version specification
> to say that `Relay=1` specifically denotes TAP.


### Phase 1, Item 2: Make TAP keys optional in directory documents.

(All items in phase 1 can happen in parallel.)

In C tor and Arti, we should make the `onion-key` entry
and the `onion-key-crosscert` entry optional.
(If either is present, the other still must be present.)

When we do this, we should also modify the authority code
to reject any descriptors that do not have these fields.
(This will be needed so that existing Tor instances do not break.)

In the microdescriptor documents format, we should make
the _object_ of the `onion-key` element optional.
(We do not want to make the element itself optional,
since it is used to delimit microdescriptors.)

We use new protocol version flags (Desc=3, Microdesc=3)
to note the ability to parse these documents.

### Phase 1, Item 3: Publish dummy TAP keys

(All items in phase 1 can happen in parallel.)

Even after step 2 is done,
many clients and relays on the network
will still require TAP keys to be present in directory documents.
Therefore, we can't remove those keys right away.

Relays, therefore, must put _some_ kind of RSA key
into their `onion-key` field.

I'll present three designs on what relays should do.
We should pick one.

> #### Option 1 (conservative)
>
> Maybe, we should say that a relay
> should generate a TAP key, generate an onion-key-crosscert,
> and then discard the private component of the key.

> #### Option 2 (low-effort)
>
> In C tor, we can have relays simply proceed as they do now,
> maintaining TAP private keys and generating crosscerts.
>
> This has little real risk beyond what is described in Option 1.

> #### Option 3 (nutty)
>
> We _could_ generate a global, shared RSA1024 private key,
> to be used only for generating onion-key-crosscerts
> and placing into the onion-key field of a descriptor.
>
> We would say that relays publishing this key MUST NOT
> actually handle any TAP requests.
>
> The advantage of this approach over Option 1
> would be that we'd see gains in our directory traffic
> immediately, since all identical onion keys would be
> highly compressible.
>
> The downside here is that any client TAP requests
> sent to such a relay would be decryptable by anybody,
> which would expose long-obsolete clients to MITM attacks
> by hostile guards.

We would control the presence of these dummy TAP keys
using a consensus parameter:

`publish-dummy-tap-key` —  If set to 1, relays should include a dummy TAP key
in their routerdescs.  If set to 0, relays should omit the TAP key
and corresponding crosscert. (Min: 0, Max, 1, Default: 0.)

We would want to ensure that all authorities voted for this parameter as "1"
before enabling support for it at the relay level.


### Phase 2, Item 1: Allow TAP-free routerdescs at the authorities

Once all clients and relays have updated to a version
where the `onion-key` router descriptor element is optional
(see phase 1, item 2),
we can remove the authority code that requires
all descriptors to have TAP keys.

### Phase 2, Item 2: Generate TAP-free microdescriptors

Once all clients and descriptors have updated to a version
where the `onion-key` body is optional in microdescriptors
(see phase 1, item 2),
we can add a new consensus method
in which authorities omit the body when generating microdescriptors.

### Phase 3: Stop publishing dummy TAP keys.

Once all authorities have stopped requiring
the `onion-key` element in router descriptors
(see phase 2, item 1),
we can disable the `publish-dummy-tap-key` consensus parameter,
so that relays will no longer include TAP keys in their router descriptors.


[proposal 216]: ./216-ntor-handshake.txt
[proposal 245]: ./245-tap-out.txt
[hsv2-deprecation]: https://support.torproject.org/onionservices/v2-deprecation/
[relays in Arti]: https://gitlab.torproject.org/tpo/team/-/wikis/Sponsor%20141
